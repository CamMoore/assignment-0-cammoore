package edu.usu.cs.oo;

public class Company {

	private String name;
	private Location location;
	
	public Company()
	{
		name = "";
		location = new Location();
	}
	public Company(String nam, Location loc)
	{
		name = nam;
		location = loc;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Location getLocation() {
		return location;
	}
	public void setLocation(Location location) {
		this.location = location;
	}
	public String toString()
	{
		String info = "\tCompany: " + name + "\n";
		info += "\tAddress: "+ "\n" + location.toString();
		return info;
	}
}
