package edu.usu.cs.oo;

public class Location {

	private String streetAddress;
	private String areaCode;
	private String city;
	private String state;
	
	Location()
	{
		streetAddress = "";
		areaCode = "";
		city = "";
		state = "";
	}
	
	Location(String street, String zip, String town, String states)
	{
		streetAddress = street;
		areaCode = zip;
		city = town;
		state = states;		
	}
	
	public String getStreetAddress()
	{
		return streetAddress;
	}
	
	public String getAreaCode()
	{
		return areaCode;
	}
	
	public String getCity()
	{
		return city;
	}
	
	public String getState()
	{
		return state;
	}
	
	public void setStreetAddress(String street)
	{
		streetAddress = street;
	}
	
	public void setAreaCode(String zip)
	{
		areaCode = zip;
	}
	
	public void setCity(String town)
	{
		city = town;
	}
	
	public void setState(String st)
	{
		state = st;
	}
	public String toString()
	{
		String address = "\t\t" + streetAddress + "\n\t\t" + city + ", " + state + "\n\t\t" + areaCode;
		return address;
	}
}
