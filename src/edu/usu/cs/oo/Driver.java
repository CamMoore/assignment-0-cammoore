package edu.usu.cs.oo;

public class Driver {
	
	public static void main(String[] args)
	{
		Student student = new Student("Chris Thatcher", "AOneMillion", new Job("Dentist", 100000, new Company()));
		
		Student me = new Student("Cam Moore", "A1", new Job("Professional Gamer", 100000000, new Company("Some Game Company", new Location("1 My Street", "99999", "My Town", "My State"))));
		
		
		System.out.println(student);
		System.out.println();
		System.out.println(me);
		/*
		 * Print out the student information. 
		 */
	}

}